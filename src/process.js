import React, {useState} from 'react';
import './App.css';
import { Button } from 'reactstrap';

function Process() {
    return(
        <div>
            <div className="container">
                <p className="process-title display-4">Process</p>
                <hr style={{paddingTop: "0px", width:'200px', height:'5px', /* Permalink - use to edit and share this gradient: https://colorzilla.com/gradient-editor/#43d8c9+31,7db9e8+100 */
                background: "#43d8c9", /* Old browsers */
                background: "-moz-linear-gradient(left,  #43d8c9 31%, #7db9e8 100%)", /* FF3.6-15 */
                background: "-webkit-linear-gradient(left,  #43d8c9 31%,#7db9e8 100%)", /* Chrome10-25,Safari5.1-6 */
                background: "linear-gradient(to right,  #43d8c9 31%,#7db9e8 100%)", /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
                filter: "progid:DXImageTransform.Microsoft.gradient( startColorstr='#43d8c9', endColorstr='#7db9e8',GradientType=1 )" /* IE6-9 */
                
                }}/>
        </div>
        <div className="process-cards">
                <div className="process-card">
                  <div className="card-container">
                    <h4><b><span className="process-card-title">Make a Call</span></b></h4> 
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p> 
                  </div>
                </div>
                <div className="process-card">
                        <div className="card-container">
                          <h4><b><span className="process-card-title">Select a plan</span></b></h4> 
                          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p> 
                        </div>
                      </div>
                <div className="process-card">
                  <div className="card-container">
                    <h4><b><span className="process-card-title">Installation Process</span></b></h4> 
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p> 
                  </div>
                </div>

                <div className="process-card">
                        <div className="card-container">
                          <h4><b><span className="process-card-title">Payment Process</span></b></h4> 
                          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p> 
                        </div>
                      </div>
                
        </div>

        </div>
        
            
    )
} 

export default Process