import React, {useState} from 'react';
import logo from './logo.svg';
import './App.css';
import { Button, Navbar } from 'reactstrap';
import Footer from './footer'
import About from './about';
import Services from './services';
import Process from './process';
import HeadSection from './headSection';
import NavbarHead from './navbar'

function App() {

 
  

  return (
    <div className="App">
      
      <NavbarHead></NavbarHead>

      <div className="call-to-action">
          <img src="https://img.icons8.com/carbon-copy/100/000000/phone.png"/>
      </div>

      <HeadSection></HeadSection>

      <div className="process-container">
        <Process></Process>
        <Button variant="primary" >Contact Us</Button>
        <div className  ="container">
        </div>
        <br/>

        <About></About>
      </div>
      
      <Services></Services>          
      <Footer></Footer>


    </div>
      
  );
}

export default App;
