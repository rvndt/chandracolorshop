import React, {useState} from 'react';
import './App.css';
import { Button } from 'reactstrap';

function Footer() {
    return(
        <div className="footer">
            
                    <form className="contact-us-form">
                            <p className="process-title display-4">Contact Us</p>
                            <div className="form-group">
                              <label htmlFor="exampleFormControlInput1">Email address</label>
                              <input type="email" className="form-control" id="exampleFormControlInput1" placeholder="name@example.com"/>
                            </div>
                            <div className="form-group">
                            <label htmlFor="exampleFormControlTextarea1">Message</label>
                            <textarea className="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
                            </div>
                    </form>
                    <div className="social-media-handle">
                        
                            <div><img src={require('./images/facebook.svg')}/></div>
                            <div><img src={require('./images/instagram.svg')}/></div>
                            <div><img src={require('./images/youtube.svg')}/></div>
                            <div><img src={require('./images/linkedin.svg')}/></div>
                            {/* <!-- <div>Icons made by <a href="https://www.flaticon.com/authors/freepik" title="Freepik">Freepik</a> from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a></div> --> */}
            
                    </div>
              </div>
    )
    
}

export default Footer

