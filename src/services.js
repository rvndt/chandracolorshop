import React, {useState} from 'react';
import './App.css';
import { Button } from 'reactstrap';

function Services() {
    return(
        <div>
            <div className="container service">
            <p className="process-title display-4">Services</p>
            <hr style={{paddingTop: "0", width:'200', height:'5', /* Permalink - use to edit and share this gradient: https://colorzilla.com/gradient-editor/#43d8c9+31,7db9e8+100 */
                background: "#43d8c9", /* Old browsers */
                background: "-moz-linear-gradient(left,  #43d8c9 31%, #7db9e8 100%)", /* FF3.6-15 */
                background: "-webkit-linear-gradient(left,  #43d8c9 31%,#7db9e8 100%)", /* Chrome10-25,Safari5.1-6 */
                background: "linear-gradient(to right,  #43d8c9 31%,#7db9e8 100%)", /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
                filter: "progid:DXImageTransform.Microsoft.gradient( startColorstr='#43d8c9', endColorstr='#7db9e8',GradientType=1 )" /* IE6-9 */
                
                
                
              }}/>
         
    </div>
    <div className="service-cards">
            <div className="service-card">
              <div className="service-card-container">
                <h4><b>New Connection</b></h4> 
                <p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p> 
              </div>
            </div>
            <div className="service-card">
                    <div className="service-card-container">
                      <h4><b>Recharge</b></h4> 
                      <p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p> 
                    </div>
                  </div>
            <div className="service-card">
              <div className="service-card-container">
                <h4><b>Services</b></h4> 
                <p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p> 
              </div>
            </div>
            </div>        
        </div>
            
    )
} 

export default Services