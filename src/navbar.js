import React, {useState} from 'react';
import './App.css';
import { Button } from 'reactstrap';

function Navbar() {

    const [cl, setcl] = useState("topnav");

    return(
        <div className={cl} id="myTopnav">
        <a href="" style = {{float:"left", fontFamily: 'enum(Pacifico, cursive)',
                    padding:'0px' }}><h1>CH</h1></a>
        <a href=""></a>
        <a href=""></a>
        <a href="#home" className="active">Home</a>
        <a href="#news">About Us</a>
        <a href="#contact">Services</a>
        <a href="#about">Contact Us</a>
        
        <a className="icon" onClick={()=>{
          console.log(cl);
          
          if (cl ==='topnav'){
            setcl("topnav"+ " responsive")
          }else{
            setcl("topnav")
          }
        }}><i className="fa fa-bars"></i>
        </a>
      </div>
    )
} 

export default Navbar