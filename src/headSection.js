import React, {useState} from 'react';
import './App.css';
import { Button } from 'reactstrap';

function HeadSection() {
    return(
        <div className="head-section jumbotron jumbotron-fluid">
        <div className="container head-section-content">
          <p className="head-section-title display-4">Communication House</p>
          
          <p className="lead">This is a simple Description Text, Lorem ipsum dolor sit amet,  eu fugiat nulla pariatur..</p>
          <hr className="my-4"/>
          <p>It uses utility classes for typography and spacing to space content out within the larger container.</p>
          <Button className="btn btn-colorpri btn-lg" color="success" >1800-208-6633</Button>
        </div>
      </div>
            
    )
} 

export default HeadSection